package com.example.poc

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


class ImageGetter(private val context : Context) : Html.ImageGetter {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getDrawable(src: String) : Drawable? {
        val id : Int = when (src) {
            "smiley1" -> R.drawable.smiley_1
            "smiley2" -> R.drawable.smiley_2
            "smiley3" -> R.drawable.smiley_3
            else -> -1
        }
        if (id == -1)
            return null
        println(id)
        val d : Drawable? =  context.getDrawable(id)
        d?.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
        return d
    }
}

class NoteActivity: AppCompatActivity() {

    private lateinit var input: EditText
    private lateinit var output : TextView
    private lateinit var boldBtn : Button
    private lateinit var italicBtn : Button
    private lateinit var underlineBtn : Button
    private val imgGetter : ImageGetter = ImageGetter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)
        input = findViewById<EditText>(R.id.textInput)
        output = findViewById<TextView>(R.id.resultText)
        boldBtn = findViewById<Button>(R.id.bold)
        italicBtn = findViewById<Button>(R.id.italic)
        underlineBtn = findViewById<Button>(R.id.underline)
        input.addTextChangedListener(watcher)
        input.setOnKeyListener(keyListener)
    }

    fun smiley1Button(view: View) {
        input.text.insert(input.selectionStart, "<img src=\"smiley1\" />")
    }

    fun smiley2Button(view: View) {
        input.text.insert(input.selectionStart, "<img src=\"smiley2\" />")
    }

    fun smiley3Button(view: View) {
        input.text.insert(input.selectionStart, "<img src=\"smiley3\" />")
    }

    fun boldButton(view: View) {
        if (input.selectionStart == input.selectionEnd) {
            input.text.insert(input.selectionStart, "<b></b>")
            input.setSelection(input.selectionEnd - 4)
        }
        else {
            input.text.insert(input.selectionStart, "<b>")
            input.text.insert(input.selectionEnd, "</b>")
        }
        boldBtn.isActivated = !boldBtn.isActivated
    }

    fun italicButton(view: View) {
        if (input.selectionStart == input.selectionEnd) {
            input.text.insert(input.selectionStart, "<i></i>")
            input.setSelection(input.selectionEnd - 4)
        }
        else {
            input.text.insert(input.selectionStart, "<i>")
            input.text.insert(input.selectionEnd, "</i>")
        }
        italicBtn.isActivated = !italicBtn.isActivated
    }

    fun underlineButton(view: View) {
        if (input.selectionStart == input.selectionEnd) {
            input.text.insert(input.selectionStart, "<u></u>")
            input.setSelection(input.selectionEnd - 4)
        }
        else {
            input.text.insert(input.selectionStart, "<u>")
            input.text.insert(input.selectionEnd, "</u>")
        }
        underlineBtn.isActivated = !underlineBtn.isActivated
    }

    private val watcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            output.text = Html.fromHtml(s.toString(), imgGetter, null)
        }
    }

    private val keyListener = object : View.OnKeyListener {
        override fun onKey(v: View?, code: Int, keyEvent: KeyEvent?): Boolean {
            if (keyEvent?.action == 0 && code == 66) {
                println("test")
                input.text.insert(input.selectionStart, "<br />")
                return true
            }
            return false
        }
    }
}