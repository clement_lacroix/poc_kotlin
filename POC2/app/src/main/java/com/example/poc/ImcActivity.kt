package com.example.poc

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.pow

class ImcActivity() : AppCompatActivity() {

    private lateinit var weightText: EditText
    private lateinit var heightText: EditText
    private lateinit var radioGroup: RadioGroup
    private  lateinit var endInput: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imc)
        weightText = findViewById<EditText>(R.id.weightInput)
        heightText = findViewById<EditText>(R.id.heightInput)
        radioGroup = findViewById<RadioGroup>(R.id.radioGroup2);
        endInput = findViewById<TextView>(R.id.endInput)
    }

    fun calcButton(view: View) {
        if (weightText.text.isEmpty() || heightText.text.isEmpty())
            Toast.makeText(this, "Veuillez remplir les champs", Toast.LENGTH_SHORT).show();
        else {
            val weight : String = weightText.text.toString()
            val height : String = heightText.text.toString()
            var tmp: Double = if (radioGroup.checkedRadioButtonId == R.id.radioCm)
                (height.toDouble() / 100).pow(2);
            else
                (height.toDouble()).pow(2)
            val imc : Double = (weight.toDouble() / tmp)
            endInput.text = imc.toString()
        }
    }

    fun razButton(view: View) {
        weightText.text.clear()
        heightText.text.clear()
        endInput.text = "Calculez pour obtenir un résultat"
    }
}