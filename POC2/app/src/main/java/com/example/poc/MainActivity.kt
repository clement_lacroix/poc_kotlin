package com.example.poc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun buttonImc(view: View) {
        val intent = Intent(this, ImcActivity::class.java)
        startActivity(intent);
    }

    fun buttonNote(view: View) {
        val intent = Intent(this, NoteActivity::class.java)
        startActivity(intent)
    }
}